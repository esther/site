## Première réunion de travail

Date : Février 2020

Présent : Quentin, Alex, Maximilien, Vincent
Remote : Simon
Invité : Tom

> Maximilien prends des notes.

https://p.adnab.me/pad/#/2/pad/edit/VOqs46ZeH7iR2EnL63xeXxHP/

Depuis l'AG 1
-------------

Quentin (anime la réunion) :

- Migration DNS (depuis Cloudflare vers Online)
- Ajout domaine deuxfleurs.org (acheté par Maximilien)
- Quentin explique la partie technique
- Alex explique les avancées sur la partie LDAP/authentification basée sur consul (bottin + guichet)
- Ajout de l'invitation dans guichet (lien à usage unique)
- Nettoyage des comptes LDAP
- Mettre une étiquette deuxfleurs sur la boite de Quentin
- Discussion avec Jaxom & Almet sur l'hébergement
- Le site web c'est important, tout le monde en parle
- Refondre la partie graphique pour la rendre plus attrayante et moins RFC-like
- Alex s'est lancé dans du dev de bridge matrix qui fonctionne pour mattermost et XMPP
- Le bridge mattermost focntionne pas mal

Ce que l'on n'a pas encore fait
-------------------------------

Banque : la moitié des cotisations part dans une banque
Décision de faire un pot commun ?
Continuer sans ? (mais c'est dans les statuts)

> Vote : trésorerie en liquide jusqu'à 200€
> Sinon on dépense ou bien on ouvre un compte

- 200€ : contre 0, neutre 0, unanimité pour
- Fonctionner en pot commun : contre 0, neutre 0, unanimité pour

**Motion voté.**

### Gestion de la comptabilité

Quentin a un compte courant vide. Mais à son avis pas une bonne idéee.
Gestion de la comptabilité sur un logiciel (lequel ?)
Trésorier ?

Alex a trouvé une boite.

> Vote : Alex est le gardien de la boite qui contient les cotisations dans la limite de 200€
> Contre : 0, Neutre 0, Unanimité

Pour le choix du logiciel, Maximilien enverra un mail avec des solutions. L'idée de base est de mettre le fichier dans un repo git (facilement backupé et consultable), avec des commit signés.

Pour les présents, les cotisations sont payables à la fin de l'AG.

### Charte

Trouver pour la prochaine AG (voire avant) une base. Maximilien doit envoyer des idées sur la base de ce qui est fait en conférence.   Quentin envoie des idées pour les projets Open-Source.

### Site web

Intégrer la documentation au site web, afin qu'elle soit consultable et plus transparent par rapport aux infrastructures.

Outil pour build du Markdown avec un blog statique.
Utiliser les outils de templating des trucs web.

Quentin fera une proposition.
Simon : Les gens qui font des choses se doivent de les documenter.

À qui s'adresse la documentation :
- tout ce qui tourne autour de l'administration
- de l'accessibilité
- la partie technique

Répliquer le gitea d'Adrien (Maximilien va leur faire sur le sien).

### Lieux de réunion

Vincent propose le salon de thé (on peut commander un café), mais on est trop bruyant ?

Pas de souci tant que l'on rentre dans un salon de chez quelqu'un (jusqu'à 10-12 personnes)

Les objectifs
-------------

Quentin : but original du CHATON, documenter l'auto-hébergement distribué, fournir des services que tu gères toi-même, sans manipulation ni tracking

Trois niveaux :
- petits services
- backups et disaster-recovery
- CHATON (candidature chez framasoft et référencement) : l'objectif est-il d'obtenir le label ou bien simplement de s'inspirer de leur idéal ?

> Simon : pour la partie non technique, sauf si cela présente un effort technique trop important.
> Quentin : leur cahier des charges n'est pas aberrant et pourrait être un guide sur le développement de l'infra

**TODO** : faire un document de travail (Quentin a fait une milestone dans le gitea)
- géo-distribué (résilient à la perte d'une machine/d'un site - penser datacenter)

**TARGET** soumettre une candidature _CHATON_ dans 6 mois

Pour la géo-distribution, Quentin préconise le backend S3-compatible
Approche totalement différente des ressources.

Débat à suivre.

### Recommandations

Quentin : si jamais on embarque des gens et que l'on leur fait faux bond, on dessert la cause de l'hébergement participatif.

Alex : il est de la responsabilité des personnes qui créent un compte de s'informer des limites

Simon : par cooptation : chacun voit midi à sa porte.

> Vote : Maximilie propose l'ajout d'un avertissement sur le formulaire d'inscription de guichet. La rédaction du bloc de text est laissé à Maximilien, et soumise à l'approbation du prochain conseil d'afministration.
> Contre : 0, Neutre 0, Unanimité

Repasser sur le document de travail
-----------------------------------

Alex : priorité de faire le site web et d'avoir une solution facilement éditable pour les PV d'AG & co (pas tout le temps dépendre des pads)

Quentin : les PV en PDF sont stockés dans un repos

Retex de Toms
-------------

Toms est intéressé pour rejoindre l'association.
Pas assez de vison pour savoir si c'est réalisable.

Quentin montre la nouvelle maquette du site web.
