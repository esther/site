Deuxfleurs utilise les composants suivants dans son infrastructure:

- Ansible (configuration des noeuds)
- Docker (conteneurs)
- Nomad (orchestration des conteneurs)
- Consul (stockage clef/valeur distribué, découverte de services)
- Glusterfs (système de fichiers distribué)
- Stolon (système de réplication pour PostgreSQL)

Les services proposés sont les suivants:

- Chat via Matrix (Synapse, Riot)
- Email (Postfix, Dovecot, SoGo)
- Stockage (Seafile)

Par ailleurs, nous avons développé nous-même un certain nombre d'outils pour compléter la stack:

- [Bottin](https://bottin.eu), un serveur LDAP (gestion des comptes utilisateurs) basé sur le stockage clef/valeur de Consul
- [Guichet](https://git.deuxfleurs.fr/Deuxfleurs/Guichet/), une interface web de gestion des utilisateurs
- [Easybridge](https://git.deuxfleurs.fr/lx/Easybridge/), un bridge entre Matrix et d'autres réseaux
- [Diplonat](https://git.deuxfleurs.fr/Deuxfleurs/diplonat/), un outil permettant de configurer automatiquement les redirections de ports d'un routeur
- [Garage](https://git.deuxfleurs.fr/lx/garage/), un stockage d'objets distribué multi-sites implémentant un sous-ensemble de l'API Amazon S3

Le code de l'infrastructure [est publiquement disponible](https://git.deuxfleurs.fr/Deuxfleurs/deuxfleurs.fr/).
