# Assemblée Générale Constitutive

Le 13 janvier 2020 à 19 heures, les fondateurs de l'association
Deuxfleurs se sont réunis en assemblée générale constitutive au 24 rue
des Tanneurs à Rennes. Sont présents Adrien, Alex, Anaïs, Axelle,
Louison, Maximilien, Quentin, Rémi et Vincent.

L'assemblée générale désigne Adrien en qualité de président de
séance et Quentin en qualité de secrétaire de séance. Le
président de séance met à la disposition des présents le projet de
statuts de l'association et l'état des actes passés pour le compte de
l'association en formation.

Puis il rappelle que l'assemblée générale constitutive est appelée à
statuer sur l'ordre du jour suivant :

- présentation du projet de constitution de l'association ;
- présentation du projet de statuts ;
- adoption des statuts ;
- désignation des premiers membres du conseil ;
- pouvoirs en vue des formalités de déclaration et publication.

Enfin, le président de séance expose les motifs du projet de création de
l'association et commente le projet de statuts. Il ouvre la discussion.
Un débat s'instaure entre les membres de l'assemblée.

Après quoi, personne ne demandant plus la parole, le président met
successivement aux voix les délibérations suivantes.

## 1e délibération

L'assemblée générale adopte les statuts dont le projet lui a été soumis.
Cette délibération est adoptée à l'unanimité.

## 2e délibération

L'assemblée générale constitutive désigne en qualité de premiers membres
du conseil d'administration :

- Adrien
- Alex
- Maximilien
- Quentin
- Vincent

Conformément aux statuts, cette désignation est faite pour une durée
expirant lors de l'assemblée générale qui sera appelée à statuer sur les
comptes de l'exercice clos le 13 janvier 2021. Les membres du conseil
ainsi désignés acceptent leurs fonctions

Nom, prénom et signature du président et du secrétaire de séance
