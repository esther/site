# Interconnectons nos infrastructures

*Ce document est une "demande de commentaire". Amendez-le, modifiez-le, ajoutez vos réserves ou vos solutions alternatives librement.*

## Contexte

Nous avons une architecture type micro-services.
Chaque service n'est pas attaché à une machine, il peut en changer pour mieux répartir la charge ou en cas d'incident.
La plupart de ces services sont consommés en interne, leur adresse est découverte grâce au serveur DNS exposé par Consul. Ils ne sont donc pas exposés sur internet.
Principe de minimisation de la surface d'attaque : consommé en interne donc pas de raison d'être disponible en externe.
Qui plus est, la communication entre ces services se fait souvent en clair pour des raisons de simplicités.
Ce n'est pas (trop) problématique, car le réseau interne n'est pas (supposé) surveillé.

## Problème

On ne peut pas consommer les services internes d'une infrastructure A depuis une infrastructure B.
Par exemple, on voudrait que le serveur LDAP géré par Quentin soit consommable par le serveur Git géré par Adrien, avec ces propriétés :

* sans qu'il soit exposé directement sur internet,
* sans que la communication entre A et B puisse être espionnée,
* sans que le service soit "attaché" à une machine en particulier.

## Difficultés techniques

D'un point de vue topologie réseau, on a une première contrainte, le NAT en IPv4.
En effet, si on a des serveurs derrière un même NAT ils auront envie de communiquer via l'IP interne.
Mais les algos de gestion de membre (membership management) de Consul & co pourrait être empoisonné par ces IPs qui n'auraient que de valeur en interne.
Après, on peut imaginer qu'on leur donne que l'IP externe pour communiquer, et se baser sur le NAT hairpinning de la box pour les communications intra cluster, mais ça rajoute une pression inutile sur la box : tout le traffic inter cluster sera rerouté et réécrit par la box, on se retrouve avec des traitements L3 inutiles.
C'est particulièrement critique si on commence à faire du transfert de données (comme Garage par exemple).

## Être adressable sur Internet

Cette complexité devrait être évitée à mon avis. Pour cela je propose de baser nos communications de cluster via IPv6 seulement pour pouvoir adresser tout le monde directement. Je propose d'éditer un cahier des charges de la configuration minimale qu'une personne doit remplir pour s'interconnecter à l'infrastructure Deuxfleurs. Voilà une ébauche :

- Avoir une IPv6 routable sur Internet
- Ne pas avoir de filtrage de port imposé par son fournisseur d'accès (exception possible pour le port 25 en sortie...)
- Avoir au minimum 1Go de RAM
- Avoir un processeur x86

## Trouver un service et chiffrer systématiquement

Maintenant que toutes les machines de toutes les infrastructures peuvent toutes communiquer entre elles, il nous reste encore deux problèmes :
- Les services ne sont pas attachés à une machine, et donc pas attachés à une adresse réseau
- Le trafic passant en clair sur internet est supposé espionné

Il nous faut donc deux propriétés qui découlent directement de ces deux besoins :
- Un moyen de permettre à un service C de communiquer avec un service D
- Chiffrer systématiquement le traffic qui part sur internet

Voici quelques solutions auxquelles je pense :

### La *low-tech* : TLS au niveau applicatif et Consul DNS en service discovery

Étant donné que toutes les machines sont adressables en IPv6, on pourrait imaginer continuer dans la lancée actuelle et enregistrer l'IPv6 publique plutôt que l'IPv4 privée. Il faudrait s'assurer que les applications fassent elles-mêmes le TLS au niveau applicatif. Mais ça voudrait dire que les services seraient exposés sur Internet en IPv6 et que notre seule protection serait le controle d'authentification réalisée par l'application (pour l'auth) et le TLS applicatif pas oublié (pour le chiffrement - et l'auth potentiellement en mutual auth).
On pourrait imaginer upgrader ce modèle en rajoutant une règle IPv6 dans le firewall des serveurs pour autoriser le trafic IPv6 global qu'entre serveurs de l'infra. Et seuls les ports des services publics actuels seraient ouverts à tous en IPv6.

**Avantages:**

- Bye bye le NAT
- Basé sur des protocoles standard
- Conceptuellement "simple"

**Inconvénients:**

- Complexité de mise en place, car il faut s'assurer que **tous** les services internes du cluster utilisent une authentification et que celle-ci est bien configurée
- Pas de défense en profondeur, donc il restera toujours le risque d'un service mal configuré qui permet de compromettre le système
- Certains services ne savent peut-être pas faire d'authentification ? Par exemple GlusterFS (pour l'instant on ne s'en est pas débarassés) est-il capable de faire du TLS entre les noeuds ?
- Si on choisit de rajouter un firewall, sait-on le configurer automatiquement lorsque les services changent de machine/de port ?

### La *networking* : Ajouter un VPN

Solution étudiée par TeDomum/ACID. Ils partent sur Wesher. Voici leur avis :

> On s'est posé la question d'utiliser un service mesh plutôt qu'un mesh d'infrastructure. Et il se trouve que ça collait peu à notre besoin. Il y a trop de choses au-dessus pas conçues pour être à poil sur internet et qui rentreraient pas dans le service mesh.
>
> Consul est top si tu veux interconnecter des clusters k8s dans des régions différentes. Mais si tu fais un cluster étendu y'a trop de choses exposées par défaut sans tls ou sans authent sur le réseau d'infra k8s. Et trop de choses dans plein de techno où il attend une forme de l2 partagé ou une proximité réseau, même virtuelle, comme les acl par préfixe IP dans les solutions de stockage, l'allocation de préfixe d'adressage dans les ipam de la plupart des cni, etc.
>
> On pourrait probablement s'en sortir en oubliant le cluster étendu géographique et en dégainant des solutions de synchro multi clusters avec plein de petits clusters et un service mesh par-dessus. Mais c'est beaucoup plus complexe de mise en oeuvre et beaucoup plus coûteux qu'un bon vieux vpn en dessous.
>
> Bref. On veut faire simple et efficace.

Solutions possibles: Wireguard/[Wesher](https://github.com/costela/wesher), `tinc`, cjdns/yggdrasil.

**Avantages:**

- Indépendant de toute autre problématique sur le cluster
- Sans doute la solution la plus rapide à déployer à partir de l'état actuel
- Les services continuent à se parler sur un réseau normal (cf les remarques de TeDomum ci-dessus)
- Si les services communiquent en TLS et s'authentifie les uns aux autres ça fait une 2e couche de sécurité

**Inconvénient:**

- Ajoute un niveau de complexité au niveau global
- N'implémente pas de politique de sécurité entre les services du cluster
- Consommateur de CPU à haut débit
- Protocoles de routage non-standard dans le cas des protocoles à base de mesh
- (Certains clients VPN ne sont dispo que sur archi x86)

### La *micro-service architecture* : utiliser un service mesh

Consul Connect permet de reporter le problème de l'interconnexion des infrastructures non plus au niveau des applications mais au niveau du cluster. Une fois Consul et Consul Connect bien configuré, tout le trafic sera alors chiffré d'un micro service à un autre avec du TLS et de l'authentification mutuelle. Consul sera lui-même en charge de trouver comment faire communiquer les éléments.

**Avantages:**

- Solution bien intégrée avec les autres outils Hashicorp (Nomad et Consul)
- Sécurisation supplémentaire à base d'intent et d'ACL
- Gestion intégrée du nommage, du routage et de la sécurité

**Inconvénients:**

- Nécessite sans doute pas mal de travail pour le mettre en place
- Ajoute un outil potentiellement complexe et peu maîtrisé
- Les applications ne se parlent plus directement sur le réseau -> problèmes dans certains cas (par exemple Garage peut-il toujours fonctionner ?)
- Consommateur de CPU à haut débit
- Protocole encore moins standard que le VPN (en estimant que si Wireguard a atterri dans le noyau, c'est que c'est relativement standard quand même)

**Conclusion:** L'arbitrage entre la solution VPN et la solution service mesh se fait sur les deux points suivants: outil permettant de sécuriser les connexions en les autorisant au cas-par-cas (ACL+intent) vs. outil permettant de préserver un fonctionnement comme en LAN et où les applications peuvent utiliser les propriétés d'un tel réseau "classique".
