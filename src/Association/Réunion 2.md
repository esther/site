# Deuxième réunion de travail

## Lieu et date

Date : 10h le samedi 16 mai 2020

Lieu : [jitsi.deuxfleurs.fr/asso](https://jitsi.deuxfleurs.fr/asso)

## Compte rendu

Alex, Max, Adrien, Vincent, Quentin

On s'accorde sur le temps accordé aux 3 grandes parties

 - 15 min les valeurs
 - 30 min le debrief
 - 30 min les objectifs

### Discussion sur les valeurs 

Pour les valeurs, Quentin présente une liste d'objectifs qui lui semble importants.
Réorganisation de la structure sous forme des valeurs (cf page d'accueil du site web).
Essaye de donner des éléments précis.
C'est aussi une sorte de note d'intention pour plus tard pour nous aider à faire des choix.

Proposition d'Adrien :
 - notre -> la pour les valeurs

### Point sur les droits

Clarifier le process d'ajout de nouveaux membres administrateurs de l'infrastructure.

Est identifié comme relatif à l'infrastructure :
  - L'accès à l'organisation Deuxfleurs de git (infrastructure, etc.)
  - L'accès à la solution d'interconnexion des services (Consul Connect, VPN)

### Points sur les documents dits "internes"

Sont concernés:

- Liste des membres
- Comptabilité de l'asso
- Notes interne au CA concernant les procédures de gestion etc.

Créer un repo git privé dans l'orga Deuxfleurs du gitea.

### Debrief des deux mois


#### Site web

Sur ces deux derniers mois, on a déployé notre site web.
Le système est simple (pour une personne connaissant git) : on push des fichiers Markdown et le site se met à jour tout seul (generateur statique + webhook)

Design graphique perfectible : on pourrait engager un designer graphique. Mais probablement le payer.
Pour le contenu fouilli : ça évolue beaucoup et vite, Adrien trouve que ça commence à s'organiser.

**Plateforme d'hébergement de site ?** Adrien a une plateforme Wordpress en place.
Quentin préfère l'hébergement de fichiers statique. 
Quentin aimerait proposer un système basé sur l'API S3 de Garage.
  - Les utilisateur créer un bucket depuis Guichet
  - Ensuite, ils construisent leur site en local via le logiciel libre [Publii](https://getpublii.com/).
  - Ils configurent les identifiants S3 de deuxfleurs dans ce logiciel et cliquent sur "Déployer"

Les avantages de ce système : 
  - Ultra léger (sobriété numérique)
  - Sécurisé (on ne manipuler que des fichiers de notre côté)

Par contre, on est très loin de pouvoir le mettre en place.
Le système d'Adrien plus conventionnel et ne nécessitant pas d'installer de logiciel particulier est déjà disponible.

**Est ce qu'on pourrait faire un blog / aggrégateur de contenu ?**
Pour l'instant le plus simple serait de mettre un planet.
Vincent, Adrien et moi aurions des choses à dire pour un planet.
Quelques idées d'articles :

  - J'en ai marre de Google, aidez-moi !
  - Je hais mon smartphone
  - Sobriété numérique : nos réflexions (à bas les petits gestes)


#### Jitsi

Focus sur quelques points :
  - Voix aigues (femmes) qui ne passent pas (Codec low bitrate ? Firmware ? Micro)
  - Configuration du videobridge qui change (hack routing nginx)
  - Point sur l'avancement Firefox à faire à la prochaine réunion

Rémi a mis en place son jitsi également.
Alex a utilisé le Jitsi avec 15 personnes.

#### Garage

Objectif : remplacer GlusterFS trop lent / trop buggé.
Veut se faire passer pour un "vrai" système de fichiers qui pourrait être une raison des lenteurs.
À la place on voudrait cibler plus simple : un object storage.
State of the art : le plus prometteur est Minio mais restrictions bizarres.

Alex a fait de Garage son projet de confinement.
On a une version "beta" qui fonctionne avec Nextcloud.

Question qui reste en suspens : la fiabilité ?
Le modèle : les données sont répliquées 3 fois.

Si tu as des machines sur des sites/datacenters différents, tu peux les dispatcher sur différents datacenters.
À quel point la solution est fiable ?
Il faudrait faire des tests : analyser plus le code.

Soucis techniques : congestion entre datacenters.
Controler le nombre de requetes que tu envoies.

On ne voudrait pas détruire l'information directement.
Backup vs suppression de données.

Vincent pense à la suppression scabreuse de Facebook qui ne faisait que déréférancer les fichiers de l'interface mais ces derniers étaient toujours accessible publiquement via leur URL [référence](https://arstechnica.com/information-technology/2010/10/facebook-may-be-making-strides/).

Préciser la politique (30 jours, un admin peut y accéder, elles peuvent être restaurées en cas d'accident majeur).

#### [Garage Kids](https://codelyoko.fandom.com/fr/wiki/Garage_Kids) : foire à l'espace disque

Commencer les expérimentations sur Garage.

Liste de l'espace disque :
  - Quentin : 3 x 1To
  - Adrien (un NAS plus tard ?! Ses parents ?! Synology ouvert ?! [docker](https://www.synology.com/fr-fr/dsm/packages/Docker)) - rien de précis pour l'instant
  - Maximilien : 1To
  - Alex : peut libérer >1To sur serveur Kimsufi (donc en datacenter et non en auto-hébergé)

Stockage des backups :
  - Offsite ?
  - Dans Garage ?

Adrien pensait du backup avec des rsync/tar.
Adrien a commencé à faire du backup.

Maximilien met à disposition un VM pour le backup de la stack chez Quentin

#### Interconnexion

Battle Royal : VPN (supporter : Alex) vs Service Mesh (supporter : Quentin)

Contexte/But : Essayer d'avoir des machines dans différents datacenters. On voudrait les interconnecter entre elles.
Le cas d'usage c'est le LDAP, pour le consommer depuis des machines.
Wireguard est un module noyau, bonne efficacité.

Trouver comment on va expérimenter l'interconnexion.
On aurait besoin de VM derrière des NAT -> réalisme du déploiement.

La réunion se termine sur ces belles paroles :-)

---

## (Archives) Ordre du jour

- Nos valeurs :
  - protèger notre vie privée
    - économie de la surveillance
  - défendre notre liberté d'expression
    - économie de la surveillance
  - ne pas se laisser manipuler
    - économie de l'attention
  - choisir la sobriété numérique
  - prendre les décisions ensemble
  - mettre en commun nos connaissances et nos infrastructures
    - consommation excessive (obsolescence, incompatibilités, gadgets)
    - protection libertés:
      - on ne censure pas - dans les limites de la loi - ce que vous voulez partager
      - ne vous manipule pas
        - on répond à un besoin, on fournit des outils
        - on n'essaye pas d'augmenter le temps passé sur nos services
        - on ne propose pas de recommandations automatisées ou d'algorithmes "boite noire" dont le fonctionnement serait inconnu ou inexplicable
        - on valorise la transparence, tout est public par défaut (comme nos compte-rendus d'AG ou notre documentation technique)
      - promeut la sobriété numérique:
        - on réutilise du vieux matériel tant qu'on peut
        - on optimise le logiciel
  - solidaires ([définition 2 du CNRTL](https://www.cnrtl.fr/definition/solidaire), peut etre pas le bon mot)
    - choix de services grand public (jitsi plutôt que mumble, matrix plutot que IRC, etc.)
    - documentation / aide pour l'utilisation de ces services
    - valoriser et légitimer l'accompagnement humain dans l'usage des services, mis en valeur par le choix du parrainage.
  - participatif
    - mettre en commun le savoir
      - Déploiement de Jitsi
    - mettre en commun le code
      - Code publié sous license libre
    - mettre en commun les infrastructures
      - backups chez Maximilien
      - git chez Adrien
      - matrix chez Quentin
    - faire les choix collectivement, diluer le pouvoir
      - association collégiale
- Temps de discussion avec les nouveaux / invités
- Debrief des deux mois
  - Déploiement du site web
  - Déploiement et debug du Jitsi
    - Manque de doc : gestion du TURN
    - User and Developer Experience pretty bad
    - Problème avec le traitement de l'audio : voix féminines coupées
    - Succès dans mon entourage
    - Échec sur l'ADSL
    - Conclusion : la pire solution de VoIP à l'exception de toutes les autres !
  - Développement de Garage
    - Soucis de congestion entre datacenters: gestion des connexions sortantes à améliorer
    - Opérations de suppression: TODO garder les vieilles versions pour un certain temps (30 jours) pour éviter toute fausse manip
    - Ça semble fonctionner bien avec NextCloud
    - Est-on prêts à se lancer dans un test grandeur nature ?
- Interconnectons nos infrastructures. [Plus d'informations par ici](/Technique/Jalon/Interconnexion.html)
- La foire à l'espace disque : échangeons nos backups !
  - Quels espaces sont déjà disponibles ?
  - Quels projets de développement ? (Adrien se paiera un NAS, un jour. Max a fourni un serveur à installer.)
    - Mes parents (Adrien) ont un Synology. Ca sert à autre chose que le chauffage ? Ya moyen de l'exploiter ?
- Le site web
  - Problèmes :
    - UI pas responsive (ça reste lisible néanmoins)
      - Quentin : Problème réglé
    - Esthétique à améliorer : je veux des roses cyberpunk.
      - On connaît des designers motivés ?
    - Contenu fouillis, manque de contexte
      - Réfléchir à une structure
  - Quels sont les objectifs et contraintes du site ?
    - Quentin mentionnait un besoin de fonctionner sans JS, une page légère... On pourrait en discuter et mettre ça au propre ?
      - Quentin : [Comment créer un site web basse technologie](https://solar.lowtechmagazine.com/fr/2018/09/how-to-build-a-lowtech-website.html) : ici ce n'est pas l'économie d'énergie du serveur qui nous intéresse mais une compatibilité fluide avec les vieux terminaux et les mauvaises connectivités mobiles. Je sais de quoi je parle, mes parents ont un ADSL de piètre qualité.
      - Quentin : C'est d'autant plus important qu'en favorisant de vieille machines derrière des connexions domestique, on a une contrainte de départ plus forte sur le matériel et on a pas de CDN pour masquer le manque d'opti/lourdeur du site web traditionnel
    - Adrien dit : "Fuck SCSS/SASS, vive CSS"
  - Framework HTML/CSS
    - "cross-browser consistency" au minimum ([normalize.css](http://nicolasgallagher.com/about-normalize-css/))
    - responsive design & utilities ([Foundation](https://get.foundation/sites/docs/), [Pure](https://purecss.io/), Bootstrap... Adrien connaît bien Foundation, un truc comme Pure serait plus léger)
    - Quentin : À voir ce que les frameworks apportent de plus une fois un reset CSS + flexbox + media queries en place. Qui plus est, tout n'est pas configurable dans bootstrap et on se retrouve vite à empiler des hacks.
  - On fait un blog ?
    - J'ai (Adrien) quelques projets de guides et d'articles sur les libertés numériques - j'imagine que vous aussi. On pourrait proposer des articles sur blog.deuxfleurs.fr, et/ou faire un agrégateur de nos propres blogs (si vous en avez tous un, moi pas).
    - Quentin : Dans l'absolu si on peut faire le blog sur la meme plateforme que le site web, je trouverais ça bien. Sinon, un truc à penser ce serait ActivityPub pour que les gens puissent suivre le blog dans leur Mastodon par exemple, aka le Fediverse.
    - Quentin : Voir si blog peut être sur Mobilizon et déployer une instance de ce dernier donc si on veut de l'ActivityPub et qu'on peut pas l'intégrer sur le site statique.

*N'hésitez pas à compléter ce document en modifiant le fichier `src/Association/Réunion_2.md` du [dépôt du site Deuxfleurs](https://git.deuxfleurs.fr/Deuxfleurs/site)*
