# 3ème réunion de travail

**Date :** À définir, ~ 15 juillet 2020  
**Lieu :** À définir

## Objectifs d'ici la réunion

Quentin :

  * ✅ Retour Planet : est ce que c'est utile ? on peut référencer nos articles à la main dans un premier temps ? Référencer l'article StopCovid
  * ✅ Finir une v1 de Diplonat.
  * ✅ Déployer une V1 de Platoo
  * ⌛ Faire des backups sur la machine de Max.
  * ⌛ Penser à vérifier avant la réunion l'état du support de Jitsi par Firefox
    * Semblerait OK via @Vincent : https://linuxfr.org/news/firefox-76-dites-septantesix#toc-prise-en-charge-de-jitsi
  * ⌛ Email
    * ⌛ Ajouter une doc pour expliquer comment rajouter un nouveau domaine
    * ✅ Chercher des pistes pour réparer IMAP qui est lent ? 
      * Cyrus IMAP -> pas d'abstraction du stockage, Cyrus Murder pour de la replication ad hoc
      * [DarkMail](https://darkmail.info/downloads/dark-internet-mail-environment-june-2018.pdf) - ex Lavabit. Abandonné
      * [Maddy](https://foxcpp.dev/maddy/) - Caddy de l'email, pas d'abstraction sur le stockage non plus
      * Rien de probant de manière générale.
      * Propositions :
        1. Utiliser la réplication d'IMAP plutot que GlusterFS dans un premier temps
        2. Écrire un backend S3/Garage pour plus tard maybe en Go/OCaml/Rust (qui peuvent tous compiler en bibliothèque dynamique C)
  * Accepter la PR#1 d'Alex
  * Mettre à jour Consul+Nomad
  * Fix le boot du cluster
    * fstab lu avant le boot de GlusterFS 
    * nomad+consul démarrés avant le réseau
  * Peaufiner le site web et demander un retour à un·e designer graphique.
  * Écrire un article du type "Un usage plus éthique du numérique : des perspectives concrètes"
  * Écrire/transmettre un appel à contribution à la VM/Conteneur pour les tests de deuxfleurs distribué avec un cahier des charges. exemple :
    * 300 Mo de RAM
    * Linux
    * Plage de ~10 ports en UDP+TCP ouverts (peu importe lesquels mais les mentionner)
    * Une IPv4 ou une IPv6 publique (besoin que de ~10 ports dans le cas du NAT)
    * Fournir un accès SSH à ces clés : https://git.deuxfleurs.fr/Deuxfleurs/deuxfleurs.fr/src/branch/master/ansible/roles/users/files
  * Mettre à disposition une VM/Conteneur sur mon PC fixe à Lyon (pas chaud pour utiliser le cluster de prod pour les tests)
  * Faire un PoC sur Consul Connect

Alex :

  * Repo git "interne" avec la compta
  * PoC infra à base de VPN
  * Garage:
	- À coder: garder les données 30 jours lors d'une suppression
    - Tests de fiabilité
	- Déploiement beta sur le cluster, peut se configurer en TLS direct entre les datacenters (sans VPN ou Consul Connect)
	- Interface web de gestion: dans Guichet (mieux) ou à part (plus simple)
