## Créer un utilisateur Postgres

  1. Créer un compte de service dans Guichet
  2. `exec` dans un conteneur `stolon-keeper` sur une machine
  3. Executer `psql -h psql-proxy.service.2.cluster.deuxfleurs.fr`
  4. Rentrer le mot de passe
  5. Créer l'utilisateur `CREATE USER <username>;`
  6. (Optionnel) Lui créer une base de données : `CREATE DATABASE <dbname> OWNER <username>`
  7. (Optionnel) Lui donner les privileges appropriés, eg. `GRANT READ PRIVILEGES ON DATABASE <anotherdbname> TO <username>;`
  8. Vérifier les permissions : `psql -h psql-proxy.service.2.cluster.deuxfleurs.fr -U <username> <dbname>`

