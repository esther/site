## 2020-04-02 Campagne de debug Jitsi

Contact: Quentin

### Description du problème

Les conversations à 3+ donc relayées par le serveur ne fonctionnent pas bien.
Louison m'a rapporté que ça avait marché pour lui (3 utilisateurs avec un Webkit).
Mais moi ça a échoué hier soir (01/04/2020) avec des participants sous Firefox.
Le bug est toujours le même : on entend 2 personnes sur 3 ou on voit 2 personnes sur 3, on recharge la page et c'est quelqu'un d'autre pour qui ça ne fonctionne plus. Souvent c'est que dans un sens.
À chaque fois en passant sur Facebook Messenger, le problème est résolu instantanément.
Par contre Facebook Messenger impose Google Chrome/Chromium pour les visio de groupe (et ne supporte donc pas Firefox).

D'où mes 2 suspicions :

- Firefox a un bug quelconque dans sa pile WebRTC déclenché par le mode conversation de groupe
- Jitsi a un problème avec les déconnexions/changement de connexion/petit hoquets du réseau et n'arrive pas à se reconnecter. Ça pourrait être rendu pire à certain moment de la journée comme le soir où le réseau est plus sollicité. Et ce serait provoqué lors du reload on repasse de 3 à 2, en P2P donc puis de nouveau de 2 à 3.

### Approfondissement

Avant d'aller plus loin, nous avons voulu prendre le temps d'identifier précisément les problèmes d'expérience utilisateurs et leur corrélation avec la plateforme de l'utilisateur (navigateur, OS).

Pour celà, nous avons suivi deux approches :
1. Mener nos propres tests
2. Chercher d'autres retours

#### Mener nos propres tests

Nous avons effectué deux appels : un avec Firefox seulement et un avec Chrome/Chromium seulement.
Merci à Alex, Adrien et Maximilien pour leur participation.

Voilà les conclusions que nous avons tirées de nos tests :

- L'appel avec Firefox a déclenché le bug immédiatement, peu importe la version de Firefox ou de l'OS (firefox stable/nightly, fedora stable/beta, etc.)
- Le passage de tout le monde sous Chrome/Chromium a permis d'avoir une conversation stable.
- Adrien avait sa Livebox avec pare-feu configuré en mode "élevé" et a dû ajouter dans sa liste blanche les ports utilisés par Jitsi (`4443/tcp` et `10000/udp` au moment du test, seul un des deux a besoin d'être accessible)

Nous avons donc demandé à Adrien quels étaient les ports ouverts par défaut dans le mode élevé de sa box :

![Livebox Parefeu Personnalisé](Assets/livebox_parefeu_personnalise.png)

Nous avons dans un premier temps retenu le port `995/tcp` pour Jitsi, le port UDP ne pouvant être changé (limitation de Jitsi).
Cependant, pour des raisons de sécurité, les navigateurs ne peuvent pas utiliser les ports en dessous de `1024/*`, à l'exception des ports `80/tcp` et `443/tcp` comme l'indique ;'issue [#3583](https://bugs.chromium.org/p/webrtc/issues/detail?id=3583) de Chromium.

La capture n'indique pas de port TCP supérieur à 1024, nous ne pouvons donc pas résoudre ce problème de notre côté, car à l'heure actuelle, nos ports `80/tcp` et `443/tcp` sont utilisés et nous n'avons qu'une seule IP publique.
Les utilisateurs activant le parefeu en mode élevé devront ajouter une exception dans leur parefeu eux-mêmes.

#### Chercher d'autres retours

[Tedomum](https://tedomum.net/) a eu connaissance de problèmes similaires.
Ils ont également identifié Firefox et assurent qu'avec l'application Android ça marche bien.
Ils me confirment que le problème vient bien du logiciel et non pas de notre déploiement.
Ils m'ont pointé entre autres vers cette issue github [#4758](https://github.com/jitsi/jitsi-meet/issues/4758)
Apparemment une issue est dédiée en particulier au problème que nous rencontrons de déconnexion partielle d'un participant, mais nous ne l'avons pas encore retrouvée.

### Correctifs

  1. Notre instance Jitsi a été reconfigurée pour refuser Firefox. Suivre l'avancée du développement de Jitsi pour Firefox
    * [#4758](https://github.com/jitsi/jitsi-meet/issues/4758)
    * [#5439](https://github.com/jitsi/jitsi-meet/issues/5439)
    * _À compléter_
  2. Pour relayer la vidéo à travers la plupart des parefeux, notre `videobridge` Jitsi devait écouter sur le port `443/tcp`. Or ce port est déjà utilisé par notre frontal HTTPS. À défaut, Jitsi est maintenant configuré avec `8080/tcp` et `10000/udp` (contre `4443/tcp` et `10000/udp` avant).
    * Un déploiement en IPv6 pourrait résoudre le problème pour une partie des utilisateurs
    * Avoir un cluster géo-distribué avec plusieurs IPv4 pourrait également résoudre le problème
    * Avoir un frontend layer 4 (niveau TCP) qui trouve une signature pour router du DTLS vers videobridge et du TLS vers traefik

### À propos du control/data plane de Jitsi

Notre videobridge écoute donc sur les ports `8080/tcp` et `10000/udp`.

WebRTC fonctionne en deux étapes :
- Des offres doivent être échangées via un serveur de signaling quelconque
- Ensuite, ces offres servent à connecter des clients directement via un protocole TCP ou UDP avec un thin layer propre à WebRTC

#### Le control plane de Jitsi : Prosody sur HTTPS via Bosh/XMPP

Le serveur de signaling Jitsi n'est autre que le serveur de chat prosody.
Pour ça, prosody est exposé à travers HTTP grâce au protocole BOSH (XMPP overs HTTPS).
Une fois l'offre reçue ([exemple](Assets/exemple_offre.txt)), elle est enregistrée dans le navigateur à l'aide de `setRemoteDescription` pour initialiser le data plane.
On peut débugger le signaling WebRTC sous Chromium avec [chrome://webrtc-internarls](chrome://webrtc-internals/).

Quand plus de deux participants sont connectés dans la conversation, Jitsi n'envoie pas les offres de chaque participant aux autres participants. À la place, elle envoie qu'une seule offre, celle de son VideoBridge.

Ainsi, le VideoBridge est une sorte de client WebRTC particulier, qui récolte et redispatche à travers WebRTC tous les flux video/audio.

#### Le data plane de Jitsi : videobridge sur DTLS/SCTP via WebRTC

WebRTC utilise deux formats de paquets selon [Mozilla Developer Network|RTCDataChannel|DataFormat](https://developer.mozilla.org/en-US/docs/Web/API/RTCDataChannel#Data_format) :
- `UDP/DTLS/SCTP`
- `TCP/DTLS/SCTP`

On a donc un format de données arbitraire encapsulé dans du SCTP lui-même encapsulé dans du DTLS.
DTLS est prévu pour les datagrammes à l'origine, car WebRTC est prévu d'abord pour fonctionner sous UDP.
Le TCP est là en mode dégradé, en secours, il sert juste de tunnel pour relayer des datagrammes.
