# Énergie

## Consommation énergétique

(Quentin, atuin.site) Durant l'épidémie SARS-COV-2, mon appartement était vide durant un mois.
L'occasion d'estimer au plus près la consommation énergétique de mes composants à partir de la facture EDF.

Mes composants allumés durant ce mois :
  * Un réfrigérateur demi taille sous le plan de travail
  * Une freebox mini 4k
  * Un switch netgear
  * 3 Lenovo Thinkcentre M82

La facture d'avril 2020 me donne :
  * 87 kWh pour 23€

On rappelle `P = E / t`  
Sur un mois on a donc `P [en W] = E [en kWh] * 1000 / (30 [en jours] * 24 [en heures/jours])`  
Autrement dit : `P = E * 1.388889` et `E = P / 1.38889`

On estimera également à partir d'une recherche rapide sur interne que le réfrigérateur consomme environ 120 kWh par an, donc 10 kWh par mois.

Quelques déductions :
  * Prix du kWh : 0,26€
  * Énergie par mois dédiée pour deuxfleurs.fr : 77 kWh
  * Consommation instantanée équivalente : 106 W
  * Coût en électricité : 20€ 

Selon Maximilien, quelques puissances indicatives :
  * Un PC de bureau c'est entre 25 et 30 W
  * Son serveur Dell R710 c'est environ 100 W en *idle*
  * Un routeur haut de gamme c'est 13 W

Ces données sont bien cohérentes avec les résultats précédents.
On peut estimer le coût en électricité de chacun de ces appareils au passage (considérant qu'ils restent allumés le mois entier) :
  * Un Dell R710 : 100W -> 19 €/mois
  * Une tour type Lenovo Thinkcentre M82 : 28 W -> 5,2 €/mois
  * Un routeur ou un switch : 10 W -> 1,9 €/mois

Pour continuer la réflexion, il serait intéressant d'étudier la consommation des serveurs à base ARM considérant notre charge.
Cependant, avoir des tours semble être plus pertinent que des serveurs pro qui consomment toujours beaucoup comparés à une tour (bien que les performances ne soient pas comparables, bien entendu).
Idéalement, avoir des mesures directement sur les équipement permettrait d'avoir des mesures plus solides et pouvoir mieux identifier les équipements énergivores.

**Moins cher dans un datacenter ?** - Scaleway et OVH proposent des VPS à des prix compétitifs (à partir de 3 €/mois). Mettre une vieille machine en auto-hébergement peut donc couter plus cher en électricité que louer une machine chez eux. En se focalisant seulement sur ces deux points, on pourrait identifier un point de bascule puissance/énergie à partir duquel il devient plus intéressant de s'auto-héberger que d'externaliser. Avec les Lenovo Thinkcentre M82 des machines qui ont 5 ans, on est à peine gagnant : pour 5€ d'électricité par mois, une machine équivalente en *bare metal* en location dans un datacenter coûte dans les 10€ à 15€. Mais même la, la différence de prix reste faible. Je vois deux explications : 
  * du matériel qui consomme moins 
  * de l'électricité moins chère. 

**On consomme plus alors ?** - Pour avoir une approche écologique, il nous faudrait donc comparer la consommation des serveurs et non les prix finaux pratiqués par les hébergeurs. Et pour comparer le renouvellement du matériel, il faudrait comparer la consommation énergétique sur la durée de vie complète de l'appareil en y incluant **sa frabrication**.
À mon avis le bilan écologique de l'auto-hébergement n'est pas pire qu'en datacenter et pourrait même être meilleur...
