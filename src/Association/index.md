### Notre raison d'être

Aujourd'hui, de grandes entreprises conçoivent des services numériques qui ont
pour objectif de
<a href="https://fr.wikipedia.org/wiki/%C3%89conomie_de_l%27attention">maximiser le temps</a>
que nous passons dessus, de
<a href="https://fr.wikipedia.org/wiki/%C3%89conomie_de_la_surveillance">collecter et recouper des données</a>
à notre insu pour nous influencer, de
<a href="https://www.april.org/le-parlement-europeen-valide-la-generalisation-de-la-censure-automatisee">limiter nos possibilités d'expression</a>
au-delà du cadre légal et de
<a href="https://fr.wikipedia.org/wiki/Embrace,_extend_and_extinguish">créer de nouveaux monopoles</a>.
Ces effets nous montrent que la technologie n'est pas
neutre et a un réel impact sur nos vies. En choisissant et en hébergeant nos
propres outils de communication, sans but lucratif ni hégémonique, nous
espérons nous affranchir de ces nuisances et préserver nos libertés.

Pour en savoir plus, rendez-vous sur
<a href="https://www.laquadrature.net/">La Quadrature du Net</a>
et allez lire le manifeste <a href="https://chatons.org/fr/manifeste">des CHATONS</a>.

### Nos objectifs

#### Des utilisateurs impliqués

Que ce soit à l'école, par l'expérimentation, via un forum d'échange, lors d'un
atelier, via une publicité à la télévision, un tutoriel, lors d'une discussion
avec un ami, il y a toujours une phase d'apprentissage en informatique.
Malheureusement, dans ces conditions, dur de lutter pour des services libres
face à la puissance de frappe d'une entreprise et des logiciels ayant une base
d'utilisateurs immense. Nous pensons donc qu'une personne souhaitant s'héberger
chez un hébergeur indépendant a besoin d'un accompagnement. C'est pourquoi les
inscriptions se font par cooptation. La cooptation permet aussi un lien de
confiance et ainsi de se prémunir de bon nombres d'attaques que subissent les
hébergeurs.

#### Une architecture résiliente

Les sites webs, les réseaux sociaux, les emails ne peuvent fonctionner que
grâce à des ordinateurs qui restent allumés 24/24h et qui n'attendent que vous.
Cependant, ces derniers sont faillibles. Une coupure d'électricité, un disque
dur cassé, une mise à jour ratée, un bug dans le logiciel, les raisons ne
manquent pas. Heureusement, il est possible de masquer ces pannes avec du
logiciel astucieusement conçu. C'est pourquoi vous avez l'impression que Google
est toujours disponible, que Dropbox ne perd pas vos données, etc. La gestion
de ces pannes, c'est aussi ce qui rend la vie compliquée aux hébergeurs
indépendants. Entre incompréhension des utilisateurs quand un service est hors
ligne et sueurs froides pour les administrateurs, ça n'a rien de marrant. Et
c'est très chronophage. Notre objectif est donc de construire des solutions
d'hébergements qui peuvent résister à ces pannes.

<p class="center"><a href="/Technique/">En savoir plus sur l'aspect technique</a></p>
