# Infrastructure

## Physique

Un site est constitué de l'ensemble du matériel à un lieu donné géré par une (ou plusieurs) personne donnée.
Le lieu géographique peut évoluer dans le temps, comme par exemple lors d'un déménagement.
Le nommage du site est donc arbitraire, nous recommandons le choix d'un corps céleste, tout aussi étrange soit-il.
Ce découpage en sites est important pour certaines de nos applications.

### 🐢 atuin.site

Informations générales :

| Caractéristiques | Détails |
| --: | :-- |
| Administration | Alex et Quentin |
| Hébergement | 🏡 Quentin |
| Région | Bretagne |
| FAI | Free - 1Gb/s, ✅ IPv4 publique, ✅ IPv4 fixe, ✅ IPv6 publique, ✅ IPv6 fixe, ✅ SMTP, ❌ Reverse DNS |

Liste du matériel :

| Désignation | Rôle | Quantité | Détails | Image |
| -- | -- | -- | -- | -- |
| Lenovo Thinkcentre M82 | Serveur | x3 | Intel G3420 @ 3.20GHz (2 coeurs), 8Go RAM, 128GB SSD, 1TB HDD | ![photo serveur](img/lenovo.jpg)
| Netgear ?? | Switch | x1 | 24 ports @ 100 Mbit/s, 2 uplinks @ 1Gbit/s | ![photo switch](img/switch.jpg) |
| Freebox Mini 4k | Routeur | x1 | 4 ports @ 1Gbit/s, WAN Fibre 1 Gbit/s symétrique |![photo freebox](img/fbx.jpg)

### ⚔️  mars.site

Informations générales :

| Caractéristiques | Détails |
| --: | :-- |
| Administration | Adrien (et Quentin) |
| Hébergement | 🏢 Gandi |
| Région | Île-de-France |
| FAI | Gandi -  ✅ IPv4 publique, ✅ IPv4 fixe, ❓ IPv6 publique, ❓ IPv6 fixe, ❓ SMTP, ❓ Reverse DNS |

Liste du matériel :

| Désignation | Rôle | Quantité | Détails | Image |
| -- | -- | -- | -- | -- |
| VPS | Serveur | x1 | 1 vCPU, 3Go RAM, 70 GB Block Storage | N/A |

### 🪐 saturne.site

Informations générales :

| Caractéristiques | Détails |
| --: | :-- |
| Administration | Alex |
| Hébergement | 🏢 Kimsufi (filiale d'OVH) |
| Région | Hauts-de-France |
| FAI | OVH - ✅ IPv4 publique, ✅ IPv4 fixe, ✅ IPv6 publique, ✅ IPv6 fixe, ✅ SMTP, ✅ Reverse DNS |

Liste du matériel :

| Désignation | Rôle | Quantité | Détails | Image |
| -- | -- | -- | -- | -- |
| Kimsufi | Serveur | x1 | Intel Atom N2800 @ 1.86Ghz (4 coeurs), 4Go RAM, 2TB HDD, réseau 100Mbit/s | N/A |

### ✉️  mercure.site

Informations générales :

| Caractéristiques | Détails |
| --: | :-- |
| Administration | Quentin et Maximilien |
| Hébergement | 🏡 Maximilien |
| Région | Île-de-France |
| FAI | Orange - ❌ IPv4 fixe, ❌ IPv4 publique, ❌ IPv6 fixe, ✅ IPv6 publique, ❌ SMTP, ❌ Reverse DNS |

Liste du matériel :

| Désignation | Rôle | Quantité | Détails | Image |
| -- | -- | -- | -- | -- |
| VPS | Serveur | x1 | 4 vCPU, 8Go RAM, 20 GB Block Storage | N/A |

## Réseau

## Logiciel
