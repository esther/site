# Guichet

Guichet est une interface de gestion utilisateur LDAP pour Bottin.
Il vise notamment à permettre aux utilisateurs de modifier leurs identifinats ainsi que leurs données personnelles.

## Statut du développement

Guichet est actuellement utilisé en production pour deuxfleurs. Il est cependant toujours en développement actif.
Le code est ici : https://git.deuxfleurs.fr/Deuxfleurs/guichet
