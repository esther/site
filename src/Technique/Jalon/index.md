# Jalons

Les jalons définissent les grands objectifs techniques que nous souhaitons atteindre.
Ils nous servent à garder le cap quand on a la tête dans le technique tout en clarifiant nos pensées.
Nous valorisons la critique et les décisions participatives, tout le monde est donc encouragé à venir donner son avis et enrichir cette page en l'éditant directement ou en proposant une merge request.

## Vers l'auto-hébergement résilient

**Auto hébergement** : nos valeurs -> soit en justice, soit en vulgarisant, soit en proposant une alternative -> proposer une alternative nécessite de reprendre le contrôle.

**Résilient** : la course à l'uptime est veine. dans l'absolu on pourrait accepter un service qui serait down une heure par jour. le vrai enjeu, c'est la pression portée sur les administrateurs. ne pas se retrouver l'esclave de la machine. c'est LA raison pour laquelle tout le monde arrête l'auto hébergement un jour ou un autre dans sa vie.

### Pt. 1 : La vieille tour (de Babel)

10 ans d'essai, 3 *setups* différents, plus de puissance qu'ajd -> pas un pb de puissance.
Connexion ADSL, CPL.

Expérience :
  * Crash d'une carte mère x2
  * Crash d'un disque dur
  * OS cassé
  * CPL planté
  * Barrette de RAM cassée
  * Plus d'électricité
  * Problèmes de peering

Tout est un SPOF. Aller-retour Rennes-Nevers.
Diagnostique à la hâte, commande de RAM alors que MOBO broken.

### Pt. 2 : La grappe (mousse et pampre)

*Je faisais un brin de causette, le genre réservé, tu me connais. Mousse et pampre. Voilà tout d'un coup qu'un petit cave est venu me chercher, les gros mots et tout !*

La grappe de serveur, ou *cluster* dans la langue de Shakespeare.
Enlever certains points de failure.

Expérience :
  * Coupure électrique ~2h x3 

SPOF :
  * Connexion
  * Routeur
  * Switch
  * Électricité

### Pt. 3 : La foule (feat Edith Piaf)

*Emportés par la foule qui nous traîne, qui nous entraîne...*

objectif : plus de SPOF
