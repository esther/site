# site

Ce dépôt git contient les sources pour le site [deuxfleurs.fr](https://deuxfleurs.fr/).
C'est ici que vous pourrez apporter des modifications au site.

## Comment ajouter du contenu ?

Prérequis :
  - Un compte sur cette instance gitea.
  - Le logiciel git d'installé. [téléchager](https://git-scm.com/downloads).

Pour commencer, assurez-vous d'être connecté.
Ensuite cliquez sur le bouton "Bifurcation" en haut à droite (*fork* en anglais).
Finalement, vous pourrez cloner votre dépôt en local :

```
git clone https://git.deuxfleurs.fr/<votre nom d'utilisateur>/site.git
``` 

Pour ajouter du contenu, vous pouvez tout simplement ajouter un fichier dans le dossier `src`.
Par exemple, le fichier `src/Association/Statuts.md` se retrouve à l'adresse `https://deuxfleurs.fr/Association/Statuts.html`.

Deux formats de fichiers différents sont supportés pour l'instant :

  - `.md` pour Markdown. [documentation](https://fr.wikipedia.org/wiki/Markdown). Rapide à apprendre, il est très pratique pour rajouter du contenu sans se poser la question de l'apparence.
  - `.pug` pour Pug. [documentation](https://pugjs.org). PugJS est une syntaxe différente pour représenter du HTML. Elle offre donc toute la souplesse de ce dernier et s'adapte plus à des mises en page complexe.

Si vous ne savez pas par où commencer, choisissez markdown !
N'hésitez pas à vous inspirer des fichiers existants.

Une fois vos modifications terminées, vous pouvez commit et push :

```
git add src/Association/MonFichier.md
git commit -a
git push
```

Ensuite, vous pouvez créer une demande d'ajout (*pull request* ou *merge request* en anglais) depuis cette adresse (ou en cliquant sur l'onglet éponyme) : https://git.deuxfleurs.fr/Deuxfleurs/site/pulls

## Comment prévisualiser mes modifications ?

Prérequis :
  - nodejs et npm d'installé. [télécharger](https://nodejs.org/en/download/)

Vous pourriez vouloir vérifier que vos modifications rendent bien avant de faire votre demande d'ajout.
Pour cela, nous utilisons un script de notre cru nommé `render.js`. Il convertit le dossier `src/` vers un site web contenu dans le dossier `static/` et peut, optionnellement, servir de serveur web.

La façon la plus simple de prévisualiser vos modifications est donc de :

```bash
npm install # installer les dépendances
LISTEN=3000 node render.js # effectuer le rendu puis démarrer un serveur web sur le port 3000
```

Votre aperçu est alors disponible à cette URL : http://127.0.0.1:3000

À chaque modification, vous pouvez arrêter la dernière commande (CTRL+C) et la relancer pour forcer le rendu.

## Comment le site est mis à jour depuis ce dépôt ?

Tout est réalisé automatiquent, aucune intervention humaine n'est nécessaire.

Derrière les coulisses, une fois que votre demande d'ajout acceptée, elle arrive sur la branche `master` de ce dépôt.
Cette opération déclenche automatiquement [un webhook gitea](https://docs.gitea.io/en-us/webhooks/) qui va alors effectuer une requete HTTP sur l'URL suivante : `https://deuxfleurs.fr/update?token=<redacted>`.
Le serveur sait alors qu'il doit rafraichir le site web.
Cette fonctionnalité est fournie par un petit outil qui s'appelle [webpull](https://git.deuxfleurs.fr/Deuxfleurs/deuxfleurs.fr/src/branch/master/docker/webpull).
Ce dernier exécute alors deux commandes : `git pull` puis `./.webpull`.
`.webpull` est en réalité un script bash contenu dans ce dépôt à la racine, dont vous pouvez inspecter le contenu.
