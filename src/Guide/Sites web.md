# Hébergement de sites web 

Vous en avez marre de faire toute votre communication associative via Facebook ? Vous voulez créer votre propre site pour raconter votre dernier road-trip ou publier vos poèmes ? Vous ne savez pas vous y prendre ? Deuxfleurs est là pour vous !

Nous vous prodiguerons conseil, guidance, et hébergement pour que vos plus belles lignes soient disponibles en ligne dans les meilleures conditions. **Vous n'avez qu'à nous contacter à coucou<img class="simple" src="/img/arobase.png" height="15"/>deuxfleurs.fr**.

## Plus en détail

Nous hébergeons gratuitement les sites dont l'adresse web ressemble à `monbeausite.deuxfleurs.fr`. Si vous souhaitez votre propre nom de domaine (par exemple `monbeausite.fr`), la location du nom de domaine sera à votre charge (~15-20€/an).

Nous sommes compétents pour installer des sites fonctionnant avec [Wordpress](https://fr.wordpress.org/). C'est un système de gestion de contenu ([CMS](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu) en anglais) qui permet de construire et d'administrer un site Internet *facilement et  sans connaissances préalables*. Si Wordpress ne vous convient pas, on déterminera ensemble une solution adaptée à vos besoins et envies.

Nous assurons enfin la gestion de **sauvegardes de données** : en hébergeant vos données chez nous, vous avez la certitude de ne pas tout perdre en cas de pépin (tel que le décès prématuré d'un disque dur).

## En conclusion 

Venez chez nous ! On vous fera un havre numérique aux petits oignons. Aider Internet à retrouver sa diversité d'antan, c'est important pour nous. On veut voir des blogs en pagaille, des réseaux sociaux délaissés, des thèmes loufoques et la mort de l'uniformisation graphique.

À terme, on demandera (sans doute) de s'inscrire à l'association pour être hébergé, mais pour le moment c'est gratuit et ouvert à tou.te.s, profitez-en !

