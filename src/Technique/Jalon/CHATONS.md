Nous souhaiterions devenir un CHATON, pour ça il faudrait au moins :

- Avoir des backups sur les services "stables"
- Avoir des performances satisfaisantes sur les services, actuellement ces services cochent les cases :
  - Jitsi
  - Riot/Matrix
  - CryptPad
  - Gitea
- Avoir un modèle plus clair / écrire plus largement comment un membre peut nous rejoindre
- Corriger les problèmes de robustesse simple de l'infrastructure :
  - Ordonancement des services au boot
    - Nomad démarre avant que le réseau soit prêt
    - Consul démarre avant que le réseau soit prêt
    - GlusterFS n'a pas démarré quand le fstab est lu, donc la partition n'est pas montée
  - Reconfiguration du NAT / des ports dans le firewall
    - En cours via le développement de Diplonat
- Compléter cette liste en lisant la charte du site web des chatons
